// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: "",
    frameworks: ["jasmine", "@angular-devkit/build-angular"],
    plugins: [
      require("karma-jasmine"),
      require("karma-chrome-launcher"),
      require("karma-jasmine-html-reporter"),
      // [to be continuous]: use karma-coverage to compute code coverage
      require("karma-coverage"),
      // [to be continuous]: use karma-junit-reporter to generate JUnit report
      require("karma-junit-reporter"),
      require("@angular-devkit/build-angular/plugins/karma"),
    ],
    client: {
      jasmine: {
        // you can add configuration options for Jasmine here
        // the possible options are listed at https://jasmine.github.io/api/edge/Configuration.html
        // for example, you can disable the random execution with `random: false`
        // or set a specific seed with `seed: 4321`
      },
      clearContext: false, // leave Jasmine Spec Runner output visible in browser
    },
    jasmineHtmlReporter: {
      suppressAll: true, // removes the duplicated traces
    },
    // [to be continuous]: karma-coverage configuration (needs 'text-summary' to let GitLab grab coverage from stdout)
    coverageReporter: {
      dir: require("path").resolve("reports"),
      subdir: ".",
      reporters: [{ type: "lcovonly" }, { type: "text-summary" }],
    },
    // [to be continuous]: karma-junit-reporter configuration (report needs to be in 'reports/junit_test_report.xml')
    junitReporter: {
      outputDir: require("path").resolve("reports"),
      outputFile: "junit_test_report.xml",
      useBrowserName: false,
    },
    reporters: ["progress", "kjhtml"],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ["Chrome"],
    singleRun: false,
    restartOnFileChange: true,
  });
};
