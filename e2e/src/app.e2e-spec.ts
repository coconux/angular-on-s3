// This code will document and explain the specifics of the code provided
// Angular End to End Testing : code written by Dhormale
//  https://gist.github.com/dhormale/90dd6288ab1780bf0f3039c7cef386c9#file-app-e2e-spec-ts

import { browser, element, by, ElementFinder, ElementArrayFinder } from 'protractor';
import { promise, until } from 'selenium-webdriver';
import { AppPage } from './app.po';

const expectedTitle = 'Tour of Heroes';
// const expectedTitle = `${expectedH1}`;
const targetHero = { id: 15, name: 'Magneta' };
const targetHeroDashboardIndex = 3;
const nameSuffix = 'X';
const newHeroName = targetHero.name + nameSuffix;

// Hero Class

class Hero {
  id: number;
  name: string;

  // Factory methods

  // Hero from hero list <li> element.
  static async fromLi(li: ElementFinder): Promise<Hero> {
    const stringsFromA = await li.all(by.css('a')).getText();
    const strings = stringsFromA[0].split(' ');
    return { id: +strings[0], name: strings[1] };
  }

  // Hero id and name from the given detail element.
  static async fromDetail(detail: ElementFinder): Promise<Hero> {
    // Get hero id from the first <div>
    const strId = await detail.all(by.css('div div')).first().getText();
    // Get name from the h2
    const strName = await detail.element(by.css('h2')).getText();
    return {
      id: parseInt(strId.substr(strId.indexOf(' ') + 1), 10),
      name: strName.substr(0, strName.lastIndexOf(' '))
    };
  }
}

describe('Tour of Heroes', () => {

  let page: AppPage;

  beforeAll(async () => {
    page = new AppPage();
    await page.navigateTo();
  });

  describe('Initial page', () => {

    it(`has title '${expectedTitle}'`, async () => {
      expect(await browser.getTitle()).toEqual(expectedTitle);
    });

    it(`has h1 '${expectedTitle}'`, async () => {
      expect(await page.getTitleText()).toEqual(expectedTitle);
    });

    it(`has dashboard link`, async () => {
      expect(await page.getDashboardButton().isPresent()).toBeTruthy();
    });

    it(`has heroes link`, async () => {
      expect(await page.getHeroesButton().isPresent()).toBeTruthy();
    });

  });

  describe('Dashboard tests', () => {

    it('has 4 top heroes', async () => {
      expect(await page.getTopHeroesLinks().count()).toEqual(4);
    });

    it(`selects and routes to ${targetHero.name} details`, dashboardSelectTargetHero);

    it(`updates hero name (${newHeroName}) in details view`, updateHeroNameInDetailView);

    it(`cancels and shows ${targetHero.name} in Dashboard`, async () => {
      await element(by.buttonText('go back')).click();
      // browser.waitForAngular(); // seems necessary to gets tests to pass for toh-pt6

      const targetHeroElt = page.getTopHeroesLinks().get(targetHeroDashboardIndex);
      expect(await targetHeroElt.getText()).toEqual(targetHero.name);
    });

    it(`selects and routes to ${targetHero.name} details`, dashboardSelectTargetHero);

    it(`updates hero name (${newHeroName}) in details view`, updateHeroNameInDetailView);

    it(`saves and shows ${newHeroName} in Dashboard`, async () => {
      await element(by.buttonText('save')).click();
      await page.getDashboardButton().click();
      // browser.waitForAngular(); // seems necessary to gets tests to pass for toh-pt6

      const targetHeroElt = page.getTopHeroesLinks().get(targetHeroDashboardIndex);
      expect(await targetHeroElt.getText()).toEqual(newHeroName);
    });

  });

  describe('Heroes tests', () => {

    // reload app to reset heroes names
    beforeAll(async () => {
      page = new AppPage();
      await page.navigateTo();
    });

    it('can switch to Heroes view', async () => {
      await page.getHeroesButton().click();
      expect(await page.getAllHeroes().isPresent()).toBeTruthy();
      expect(await page.getAllHeroes().count()).toEqual(10, 'number of heroes');
    });

    it('can route to hero details', async () => {
      await getHeroLiEltById(targetHero.id).click();

      expect(await page.getHeroDetail().isPresent()).toBeTruthy('shows hero detail');
      const hero = await Hero.fromDetail(page.getHeroDetail());
      expect(hero.id).toEqual(targetHero.id);
      expect(hero.name).toEqual(targetHero.name.toUpperCase());
    });

    it(`updates hero name (${newHeroName}) in details view`, updateHeroNameInDetailView);

    it(`shows ${newHeroName} in Heroes list`, async () => {
      await element(by.buttonText('save')).click();
      browser.waitForAngular();
      const expectedText = `${targetHero.id} ${newHeroName}`;
      expect(await getHeroAEltById(targetHero.id).getText()).toEqual(expectedText);
    });

    it(`deletes ${newHeroName} from Heroes list`, async () => {
      const heroesBefore = await toHeroArray(page.getAllHeroes());
      const li = getHeroLiEltById(targetHero.id);
      await li.element(by.buttonText('x')).click();

      expect(await page.getAllHeroes().isPresent()).toBeTruthy();
      expect(await page.getAllHeroes().count()).toEqual(9, 'number of heroes');
      const heroesAfter = await toHeroArray(page.getAllHeroes());
      // console.log(await Hero.fromLi(page.allHeroes[0]));
      const expectedHeroes = heroesBefore.filter(h => h.name !== newHeroName);
      expect(heroesAfter).toEqual(expectedHeroes);
      // expect(page.selectedHeroSubview.isPresent()).toBeFalsy();
    });

    it(`adds back ${targetHero.name}`, async () => {
      const anotherHeroName = 'Alice';
      const heroesBefore = await toHeroArray(page.getAllHeroes());
      const numHeroes = heroesBefore.length;

      await element(by.css('input')).sendKeys(anotherHeroName);
      await element(by.css('button.add-button')).click();

      const heroesAfter = await toHeroArray(page.getAllHeroes());
      expect(heroesAfter.length).toEqual(numHeroes + 1, 'number of heroes');

      expect(heroesAfter.slice(0, numHeroes)).toEqual(heroesBefore, 'Old heroes are still there');

      const maxId = heroesBefore[heroesBefore.length - 1].id;
      expect(heroesAfter[numHeroes]).toEqual({ id: maxId + 1, name: anotherHeroName });
    });

    // it('displays correctly styled buttons', async () => {
    //   element.all(by.buttonText('x')).then(buttons => {
    //     for (const button of buttons) {
    //       // Inherited styles from styles.css
    //       expect(button.getCssValue('font-family')).toBe('Arial');
    //       expect(button.getCssValue('border')).toContain('none');
    //       expect(button.getCssValue('padding')).toBe('5px 10px');
    //       expect(button.getCssValue('border-radius')).toBe('4px');
    //       // Styles defined in heroes.component.css
    //       expect(button.getCssValue('left')).toBe('194px');
    //       expect(button.getCssValue('top')).toBe('-32px');
    //     }
    //   });

    //   const addButton = element(by.css('button.add-button'));
    //   // Inherited styles from styles.css
    //   expect(await addButton.getCssValue('font-family')).toBe('Arial');
    //   expect(await addButton.getCssValue('border')).toContain('none');
    //   expect(await addButton.getCssValue('padding')).toBe('5px 10px');
    //   expect(await addButton.getCssValue('border-radius')).toBe('4px');
    // });

  });

  describe('Progressive hero search', () => {

    // navigate to the page once before all of the tests begin
    beforeAll(() => browser.get(''));

    it(`searches for 'Ma'`, async () => {
      await page.getSearchBox().sendKeys('Ma');
      browser.sleep(1000);

      expect(await page.getSearchResultItems().count()).toBe(4);
    });

    it(`continues search with 'g'`, async () => {
      await page.getSearchBox().sendKeys('g');
      browser.sleep(1000);
      expect(await page.getSearchResultItems().count()).toBe(2);
    });

    it(`continues search with 'e' and gets ${targetHero.name}`, async () => {
      await page.getSearchBox().sendKeys('n');
      browser.sleep(1000);
      expect(await page.getSearchResultItems().count()).toBe(1);
      const hero = page.getSearchResultItems().get(0);
      expect(await hero.getText()).toEqual(targetHero.name);
    });

    it(`navigates to ${targetHero.name} details view`, async () => {
      const hero = page.getSearchResultItems().get(0);
      expect(await hero.getText()).toEqual(targetHero.name);
      await hero.click();

      expect(await page.getHeroDetail().isPresent()).toBeTruthy('shows hero detail');
      const hero2 = await Hero.fromDetail(page.getHeroDetail());
      expect(hero2.id).toEqual(targetHero.id);
      expect(hero2.name).toEqual(targetHero.name.toUpperCase());
    });
  });

  async function dashboardSelectTargetHero(): Promise<void> {
    const targetHeroElt = page.getTopHeroesLinks().get(targetHeroDashboardIndex);
    expect(await targetHeroElt.getText()).toEqual(targetHero.name);
    await targetHeroElt.click();
    browser.waitForAngular(); // seems necessary to gets tests to pass for toh-pt6
    expect(await page.getHeroDetail().isPresent()).toBeTruthy('shows hero detail');
    const hero = await Hero.fromDetail(page.getHeroDetail());
    expect(hero.id).toEqual(targetHero.id);
    expect(hero.name).toEqual(targetHero.name.toUpperCase());
  }

  async function updateHeroNameInDetailView(): Promise<void> {
    // Assumes that the current view is the hero details view.
    await element(by.css('#hero-name')).sendKeys(nameSuffix);

    const hero = await Hero.fromDetail(page.getHeroDetail());
    expect(hero.id).toEqual(targetHero.id);
    expect(hero.name).toEqual(newHeroName.toUpperCase());
  }

});

function getHeroAEltById(id: number): ElementFinder {
  const spanForId = element(by.cssContainingText('li span.badge', id.toString()));
  return spanForId.element(by.xpath('..'));
}

function getHeroLiEltById(id: number): ElementFinder {
  const spanForId = element(by.cssContainingText('li span.badge', id.toString()));
  return spanForId.element(by.xpath('../..'));
}

async function toHeroArray(allHeroes: ElementArrayFinder): Promise<Hero[]> {
  const promisedHeroes = await allHeroes.map(Hero.fromLi);
  // The cast is necessary to get around issuing with the signature of Promise.all()
  return Promise.all(promisedHeroes) as Promise<any>;
}
